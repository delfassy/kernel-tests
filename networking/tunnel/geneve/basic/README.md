# Geneve Basic  Test
Basic tests for geneve: add and delete geneve, run ping and netperf on geneve device.

## How to run it
Please refer to the top-level README.md for common dependencies.

### Install dependencies
```bash
root# bash ../../../../cki_bin/pkgs_install.sh metadata
```

### Execute the test
```bash
bash ./runtest.sh
```
